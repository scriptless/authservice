/*
* NPM Imports
*/
import { Document } from 'mongoose';

export interface ISession {
    _id?: string;
    audience?: string; // "admin", "vendor" or "user"
    user?: string;
    jwt?: string; // jwt as encoded string
    date?: Date;
    expire?: Date;
}

export type ISessionDocument = ISession & Document;
