/*
* NPM Imports
*/
import { Document } from 'mongoose';

export interface IInvalidJWT {
    value?: string;
    expire?: Date;
}

export type IInvalidJWTDocument = IInvalidJWT & Document;
