export type JWT = {
    iss: string;
    sub: string; // user's database _id
    email: string;
    iat: number; // issued at
    exp: number; // expires at
    aud: string; // audience: 'vendor', 'admin', 'user'
}
