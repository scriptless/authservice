//@ts-nocheck
/*
 * NPM Imports
 */
import { Document } from 'mongoose';

export interface ICredential {
  _id?: string;
  email?: string;
  username?: string;
  password?: string;
  role?: string;
  tokens?: {
    emailCode: {
      code: string;
      expiresAt: Date;
    };
    forgotPassword: {
      code: string;
      expiresAt: Date;
    };
  };
  logins?: {
    date: Date;
    ipAddress: string;
  };
  registrationDate?: Date;

  checkPassword(password: string): boolean;
  logLogin(ipAddress: string): void;
  createSession(jwt: string, role: string, remember: boolean): Promise<any>;
  getSession(): Promise<any>;
  destroySession(): Promise<any>;
}

export type ICredentialDocument = ICredential & Document;
