import * as validator from 'validator';

export const MIN_PASSWORD_LENGTH = 8;
export const MAX_PASSWORD_LENGTH = 64;

export const isEmail = (string: string) => {
    return validator.isEmail(string) && !validator.isEmpty(string);
}

export const isPassword = (string: string) => {
    return validator.isLength(string, { min: MIN_PASSWORD_LENGTH, max: MAX_PASSWORD_LENGTH })
}

export const isMobileNumber = (string: string) => {
    const locale : ValidatorJS.AlphaLocale = "de-DE";
    return validator.isMobilePhone(string, locale);
}

export const isName = (string: string) => {
    const regex = /^[a-zA-Z\u0080-\uFFFF\s\D]+$/;
    return regex.test(string);
}

export const isUsername = (string: string) => {
    const regex = /^[a-zA-Z0-9_]+$/;
    return regex.test(string);
}

export const normEmail = (email: string) => {
    return validator.normalizeEmail(email);
}

export const ignoreCaseSensitivity = (string: string) : Object => {
    return { $regex: new RegExp('^'+ string + '$', "i") };
}