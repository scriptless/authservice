/*
* Internal Imports
*/
import { extractJWT } from '../helper/jwt_helper';
import { Roles } from '../models/credential';
import { CredentialScheme } from '../models/credential';
import { log } from 'util';

export const userAuthenticator = async (req: any, res: any, next: any) => {

    req.authenticated = false;
    req.jwt = null;
    req.role = null;
    req.user = null;
    req.email = null;

    const user = await jwt(req);

    if(user !== null) {
        const doesExist = await exist(user.dec.sub);
        if(!doesExist) {
            next();
            return;
        }
        req.authenticated = true;
        req.jwt = user.dec;
        req.email = user.dec.email;
        req.role = user.role;
        req.user = user.dec.sub;
        log("User " + req.user + " authenticated as " + req.role + " (email: " + req.email + ")");
    }

    next();
}

export const jwt = async (req: any) => {
    for (let role in Roles) {
        const { dec, token } = await extractJWT(req, role);
        if (!dec || !token) continue;
        return { dec, token, role };
    }
    return null;
};

export const exist = async (_id: string) => {
    return await CredentialScheme.findOne({ _id }).then(exist => {
        if(!exist) {
            const user = new CredentialScheme({ _id });
            user.destroySession();
        }
        return exist != null;
    })
}