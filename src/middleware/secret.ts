/*
* NPM Imports
*/
import * as dotenv from 'dotenv';

dotenv.config();

export const secretAuthenticator = async (req: any, res: any, next: any) => {

    if(req.get('Auth-Secret') === process.env.AUTH_SECRET) {
        next();
        return;
    }

    return res.status(401).send({
        success: false,
        message: 'Secret key does not match'
    });

}