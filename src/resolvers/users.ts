/*
* Internal Imports
*/
import { Credential, CredentialScheme, Roles } from '../models/credential';

export const users = async (req: any) => {

    if(!req.authenticated || req.role !== Roles.ADMIN)
        return new Error("Authentication failed");

    return await CredentialScheme.find().then(res => {
        let userlist: any = [];

        res.map(async (user: Credential) => {
            const UserDetails = {
                _id: user._id,
                username: user.username,
                email: user.email,
                role: user.role,
            };

            userlist.push(UserDetails);
        });

        return userlist;
    });
}