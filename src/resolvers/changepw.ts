//@ts-nocheck
/*
 * Internal Imports
 */
import { Credential, CredentialScheme } from '../models/credential';
import { isPassword } from '../validator';
import { hashPassword } from '../helper/argon2_helper';
import { log } from 'util';
import { verifyCaptcha } from '../helper/recaptcha_helper';

export const changePassword = async (
  req: any,
  password: string,
  captcha: string
) => {
  // @ts-ignore
  if (!req.authenticated) {
    return new Error('Not authenticated');
  }

  const correctCaptcha = await verifyCaptcha(captcha, req.ip);

  if (!correctCaptcha) {
    return new Error('Captcha is wrong');
  }

  if (!isPassword(password)) {
    return new Error('Password does not meet requirements.');
  }

  const userId = req.user;
  const hash = await hashPassword(password);

  return await CredentialScheme.updateOne(
    { _id: userId },
    {
      $set: {
        password: hash,
      },
    }
  ).then(async (user: Credential) => {
    if (!user) return new Error('Authentication failed');
    log('User ' + userId + ' (email: ' + req.email + ') changed password');
    return true;
  });
};
