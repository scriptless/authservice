/*
* NPM Imports
*/
import * as dotenv from 'dotenv';
/*
* Internal Imports
*/
import { CredentialScheme } from '../models/credential';
import { ignoreCaseSensitivity } from "../validator";
import { log } from 'util';

dotenv.config();

export const deleteAccount = async (req: any) => {

    // @ts-ignore
    if(!req.authenticated)
        return new Error("Not authenticated");

    const userId = req.user;

    return await CredentialScheme.findOne({ _id: userId })
    .then(user => {
        if(!user) return new Error("User not found");
        user.destroySession();
        user.remove();
        log("User " + userId + " (email: " + req.email + ") deleted his account");
        return true;
    })
    .catch(err => {
        console.log(err)
        return new Error(err);
    }) 
}

export const deleteAccountElse = async (req: any, id: string) => {

    // @ts-ignore
    if(!req.authenticated || req.role !== Roles.ADMIN)
        return new Error("Authentication failed");

    return await CredentialScheme.findOne({$or:[{email: ignoreCaseSensitivity(id)},{_id:id}]})
    .then(user => {
        if(!user) return new Error("User " + id + " not found");
        user.destroySession();
        user.remove();
        log("User " + id + " account has been deleted");
        return true;
    })
    .catch(err => {
        console.log(err)
        return new Error(err);
    }) 
}