/*
* NPM Imports
*/
import * as dotenv from 'dotenv';

/*
* Internal Imports
*/
import { Credential, CredentialScheme, Roles } from '../models/credential';
import { isEmail, isPassword } from "../validator";
import { createJWT } from '../helper/jwt_helper';
import { randomToken } from '../helper/utils';
import { log } from 'util';
import { sendMailTemplate } from '../helper/mail_helper';

dotenv.config();

export const login = async (req: any, username: string, password: string, remember: boolean = false) => {

    if(!isEmail(username)) {
        if(!process.env.USERNAME_LOGIN)
            return new Error("Authentication failed");
    }

    if(!isPassword(password)) {
        return new Error("Authentication failed");
    }

    return await CredentialScheme.findOne().or([{ email: username }, { username: username }]).then(async(user: Credential) => {
        if(!user) {
            return new Error("Authentication failed");
        }

        if(!(<any>Object).values(Roles).includes(user.role)) {
            return new Error("Internal server error (Code: 001)");
        }

        const validPassword = await user.checkPassword(password);
        if(!validPassword) {
            return new Error("Authentication failed");
        }

        if(user.tokens.emailCode.code !== null) {

            // if user is not email-verified and email code expired
            if(user.tokens.emailCode.expiresAt < new Date()) {

                const token = randomToken();
                const date = new Date();
                date.setHours(date.getHours() + 6);

                // sends new registration mail with token
                //sendRegistrationMail(email, token)

                // send registration mail with token
                sendMailTemplate(user.email, process.env.MAILGUN_TEMPLATE_REGISTRATION_SUBJECT, process.env.MAILGUN_TEMPLATE_REGISTRATION, [{name: "token", value: token}]);


                return await CredentialScheme.updateOne({ email: user.email }, 
                { $set: 
                    { 
                        'tokens.emailCode.code': token,
                        'tokens.emailCode.expiresAt': date
                    } 
                })
                .then(_ => {
                    return new Error("Please verify your email address. A new code has been sent.")
                })
                .catch(err => {
                    console.log(err)
                    return new Error(err);
                })  
            } else {
                return new Error("Please verify your email address");
            }
        }

        // check for existing login and revoke it
        await user.destroySession();

        // save login
        const ip_address = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress;

        if (ip_address.constructor.name === "String") {
            user.logLogin(ip_address as string)
        } else {
            // array
            user.logLogin(ip_address[0])
        }

        const token = await createJWT(user._id, user.email, user.role);
        // create session
        const cookie = await user.createSession(token, user.role, remember);

        log("User " + user._id + " (email: " + user.email + ") logged in");

        return { cookie };
    });
}