//@ts-nocheck
/*
 * NPM Imports
 */
import * as dotenv from 'dotenv';
/*
 * Internal Imports
 */
import { Credential, CredentialScheme } from '../models/credential';
import {
  isEmail,
  isPassword,
  normEmail,
  ignoreCaseSensitivity,
} from '../validator';
import { hashPassword } from '../helper/argon2_helper';
import { randomToken } from '../helper/utils';
import { log } from 'util';
import { verifyCaptcha } from '../helper/recaptcha_helper';
import { sendMailTemplate } from '../helper/mail_helper';

dotenv.config();

export const resetRequest = async (
  req: any,
  email: string,
  captcha: string
) => {
  const correctCaptcha = await verifyCaptcha(captcha, req.ip);

  if (!correctCaptcha) {
    return new Error('Captcha is wrong');
  }

  if (!isEmail(email)) {
    return new Error('Please enter an email adress.');
  }

  const date = new Date();
  date.setHours(date.getHours() + 6);

  return await CredentialScheme.findOne({
    email: ignoreCaseSensitivity(email),
  }).then(async (existing: Credential) => {
    if (!existing) {
      return new Error('User does not exists.');
    }

    const token = randomToken();
    const emailAdress = normEmail(email);

    // send reset password mail with token
    sendMailTemplate(
      email,
      process.env.MAILGUN_TEMPLATE_RESETPW_SUBJECT,
      process.env.MAILGUN_TEMPLATE_RESETPW,
      [{ name: 'token', value: token }]
    );

    return await CredentialScheme.updateOne(
      { email: emailAdress },
      {
        $set: {
          'tokens.forgotPassword.code': token,
          'tokens.forgotPassword.expiresAt': date,
        },
      }
    )
      .then((_) => {
        log(
          'User ' +
            existing._id +
            ' (email: ' +
            existing.email +
            ') sent request to reset password'
        );
        return true;
      })
      .catch((err) => {
        console.log(err);
        return new Error(err);
      });
  });
};

export const resetConfirm = async (
  req: any,
  token: string,
  password: string
) => {
  if (!isPassword(password)) {
    return new Error('Password does not meet requirements.');
  }

  return await CredentialScheme.findOne({
    'tokens.forgotPassword.code': token,
  }).then(async (user: Credential) => {
    if (!user) return new Error('Token invalid.');

    if (
      user.tokens.forgotPassword !== null &&
      user.tokens.forgotPassword.expiresAt < new Date()
    ) {
      return new Error('Token expired.');
    }

    await CredentialScheme.updateOne(
      { email: user.email },
      {
        $set: {
          'tokens.forgotPassword.code': null,
          'tokens.forgotPassword.expiresAt': Date.now(),
        },
      }
    );

    const hash = await hashPassword(password);

    return await CredentialScheme.updateOne(
      { email: user.email },
      {
        $set: {
          password: hash,
        },
      }
    ).then(() => {
      log(
        'User ' +
          user._id +
          ' (email: ' +
          user.email +
          ') succesfully reset password'
      );
      return true;
    });
  });
};
