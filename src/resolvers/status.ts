export const status = async (req: any, res: any) => {

    if(!req.authenticated)
        return res.status(200).send({
            success: false,
            message: 'Authentication failed'
        })

    return res.status(200).send({
        success: true,
        data: req.jwt
    });
}