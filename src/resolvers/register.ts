/*
* NPM Imports
*/
import * as dotenv from 'dotenv';
/*
* Internal Imports
*/
import { Credential, CredentialScheme, Roles } from '../models/credential';
import { isEmail, isPassword, isUsername, normEmail, ignoreCaseSensitivity } from "../validator";
import { hashPassword } from '../helper/argon2_helper';
import { randomToken } from '../helper/utils';
import { log } from 'util';
import { verifyCaptcha } from '../helper/recaptcha_helper';
import { sendMailTemplate } from '../helper/mail_helper';

dotenv.config();

export const register = async (req: any, email: string, username: string, password: string, captcha: string) => {

    const correctCaptcha = await verifyCaptcha(captcha, req.ip)

    if(!correctCaptcha) {
        return new Error("Captcha is wrong")
    }

    const USERNAME_LOGIN: boolean = process.env.USERNAME_LOGIN === "true";

    if(!isEmail(email)) {
        return new Error("Please enter an email adress.");    
    }

    if(USERNAME_LOGIN) {
        if(!isUsername(username)) {
            return new Error("Username does not meet requirements.");
        }
    } else {
        username = undefined;
    }

    if(!isPassword(password)) {
        return new Error("Password does not meet requirements.");
    }

    const hash = await hashPassword(password);
    const date = new Date();
    date.setHours(date.getHours() + 6);

    return await CredentialScheme.findOne({ email: ignoreCaseSensitivity(email) }).then(async(existing: Credential) => {
        if(existing) {
            return new Error("User already exists.");
        }

        if(USERNAME_LOGIN) {
            if(!isUsername(username)) {
                return new Error("Username does not meet requirements.");
            }

            const exists = await CredentialScheme.findOne({
                username: ignoreCaseSensitivity(username)
            }).then(async (existing: Credential) => {
                return existing;
            });

            if(exists) return new Error("Username already exists.");
        }

        const token = randomToken();

        // send registration mail with token
        sendMailTemplate(email, process.env.MAILGUN_TEMPLATE_REGISTRATION_SUBJECT, process.env.MAILGUN_TEMPLATE_REGISTRATION, [{name: "token", value: token}]);

        const emailAdress = normEmail(email);

        const user = new CredentialScheme({
            email: emailAdress,
            username: username,
            password: hash,
            role: Roles.USER,
            'tokens.emailCode.code': token,
            'tokens.emailCode.expiresAt': date
        });

        return user.save()
            .then(res => {
                log("User " + res._id + " registered with email " + res.email + " and username " + res.username);
                return res.email;
            })
            .catch(err => {
                return new Error(err);
            })
        });
}

export const confirmEmail = async (req: any, token: string) => {

    return await CredentialScheme.findOne({ 'tokens.emailCode.code': token }).then(async(user: Credential) => {
        if(!user) return new Error("Token invalid");

        if(user.tokens.emailCode !== null && user.tokens.emailCode.expiresAt < new Date()) {
            return new Error("Token expired.");
        }

        log("User " + user._id + " (email: " + user.email + ") confirmed email address");

        return await CredentialScheme.updateOne({ email: user.email }, 
        { $set: 
            { 
                'tokens.emailCode.code': null,
                'tokens.emailCode.expiresAt': Date.now()
            } 
        }).then(() => { return true }).catch(err => {
            return new Error(err);
        });

    }).catch(err => {
        return new Error(err);
    })
}