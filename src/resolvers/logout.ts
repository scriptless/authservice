/*
* Internal Imports
*/
import { CredentialScheme } from '../models/credential';
import { log } from 'util';

export const logout = async (req: any) => {

    // @ts-ignore
    if(!req.authenticated) {
        return new Error("Not authenticated");
    }

    const user = new CredentialScheme({
        // @ts-ignore
        _id: req.jwt.sub,
    });

    const cookie = await user.destroySession();

    log("User " + user._id + " (email: " + user.email + ") logged out");

    return { cookie }
}