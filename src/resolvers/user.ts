/*
* Internal Imports
*/
import { Credential, CredentialScheme } from '../models/credential';

export const user = async (req: any) => {

    if(!req.authenticated)
        return new Error("Authentication failed");

    const userId = req.user;

    return await CredentialScheme.findOne({ _id: userId}).then(async(user: Credential) => {
        if(!user) return new Error("Authentication failed");
        
        const UserDetails = {
            _id: user._id,
            username: user.username,
            email: user.email,
            role: user.role,
            registrationDate: user.registrationDate,
            logins: user.logins
        };

        return ({ 
            user: UserDetails
        });
    });
}