//@ts-nocheck
import * as express from 'express';
import * as dotenv from 'dotenv';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as fs from 'fs';

import { DBConnector } from './helper/db_connector';

import { userAuthenticator } from './middleware/authenticated';
import { secretAuthenticator } from './middleware/secret';

import { login } from './resolvers/login';
import { logout } from './resolvers/logout';
import { register, confirmEmail } from './resolvers/register';
import { resetRequest, resetConfirm } from './resolvers/resetpw';
import { deleteAccount, deleteAccountElse } from './resolvers/delete';
import { changePassword } from './resolvers/changepw';
import { status } from './resolvers/status';
import { user } from './resolvers/user';
import { users } from './resolvers/users';

dotenv.config();

const app = express();
const { DATABASE_USER, DATABASE_PASSWORD, DATABASE_HOST, DATABASE_COLLECTION } =
  process.env;
const database = new DBConnector({
  DATABASE_USER,
  DATABASE_PASSWORD,
  DATABASE_HOST,
  DATABASE_COLLECTION,
}).connect();

(global as any).privateKey = fs.readFileSync('src/keys/private-key.pem');
(global as any).publicKey = fs.readFileSync('src/keys/public-key.pem');

app.set('trust proxy', 1);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

/*
 * Annoying CORS settings
 */
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', String(req.headers.origin));
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization, Auth-Secret'
  );
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, HEAD');
    return res.status(200).json({});
  }
  return next();
});

/*
 * TODO: Rate limit
 */

/*
 * Service Status Endpoint
 */
app.get('/', async (req: any, res: any) => {
  const { version } = require('./../package.json');
  let status = 1;
  return database
    .catch(() => (status = 0))
    .finally(() => {
      return res.status(200).send({
        status,
        name: 'Authentication Service for ' + process.env.PROJECT,
        version,
      });
    });
});

/*
 * Secret Key Middleware
 */
app.use(secretAuthenticator);

/*
 * Authentication Middleware
 */
app.use(userAuthenticator);

/*
 * Login Endpoint
 */
app.post('/login', async (req: any, res: any) => {
  if (!req.body.user || !req.body.password) {
    return res.status(400).send({
      success: false,
      message: 'Incorrect input',
    });
  }

  const { user, password, remember } = req.body;

  const data = await login(req, user, password, remember);

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });

  res.cookie('jwt_token', data.cookie.token, data.cookie.options);

  //@ts-ignore
  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * Logout Endpoint
 */
app.post('/logout', async (req: any, res: any) => {
  const data = await logout(req);

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });

  res.clearCookie('jwt_token', data.cookie.options);

  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * Confirm Email Endpoint
 */
app.post('/register/confirm/:id', async (req: any, res: any) => {
  if (!req.params.id) {
    return res.status(400).send({
      success: false,
      message: 'Incorrect input',
    });
  }

  const { id } = req.params;

  const data = await confirmEmail(req, id.trim());

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });
  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * Register Endpoint
 */
app.post('/register', async (req: any, res: any) => {
  const USERNAME_LOGIN: boolean = process.env.USERNAME_LOGIN === 'true';

  if (
    !req.body.password ||
    !req.body.email ||
    (!req.body.username && USERNAME_LOGIN) ||
    !req.body.captcha
  ) {
    return res.status(400).send({
      success: false,
      message: 'Incorrect input',
    });
  }

  const { email, username, password, captcha } = req.body;

  const data = await register(req, email, username, password, captcha);

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });
  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * ResetPw Endpoint
 */
app.post('/resetpw', async (req: any, res: any) => {
  if (!req.body.email || !req.body.captcha) {
    return res.status(400).send({
      success: false,
      message: 'Incorrect input',
    });
  }

  const { email, captcha } = req.body;

  const data = await resetRequest(req, email, captcha);

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });
  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * ResetPw Confirm Token Endpoint
 */
app.post('/resetpw/confirm', async (req: any, res: any) => {
  if (!req.body.token || !req.body.password) {
    return res.status(400).send({
      success: false,
      message: 'Incorrect input',
    });
  }

  const { token, password } = req.body;

  const data = await resetConfirm(req, token, password);

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });
  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * ChangePw Endpoint
 */
app.post('/changepw', async (req: any, res: any) => {
  if (!req.body.password || !req.body.captcha) {
    return res.status(400).send({
      success: false,
      message: 'Incorrect input',
    });
  }

  const { password, captcha } = req.body;

  const data = await changePassword(req, password, captcha);

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });
  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * Delete Endpoint
 */
app.post('/delete', async (req: any, res: any) => {
  const data = await deleteAccount(req);

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });
  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * Delete Endpoint
 */
app.post('/delete/:id', async (req: any, res: any) => {
  if (!req.params.id) {
    return res.status(400).send({
      success: false,
      message: 'Incorrect input',
    });
  }

  const { id } = req.params;

  const data = await deleteAccountElse(req, id);

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });
  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * Userinfo Endpoint
 */
app.get('/user', async (req: any, res: any) => {
  const data = await user(req);

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });
  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * Userlist infos Endpoint
 */
app.get('/users', async (req: any, res: any) => {
  const data = await users(req);

  if (data instanceof Error)
    return res.status(401).send({
      success: false,
      message: data.message,
    });
  return res.status(200).send({
    success: true,
    data,
  });
});

/*
 * Status Endpoint
 */
app.get('/status', async (req: any, res: any) => {
  return status(req, res);
});

app.listen(process.env.PORT, (port, err) => {
  if (err) {
    return console.error(err);
  }
  const { version } = require('./../package.json');
  return console.log(
    'Authservice ' +
      version +
      ' for ' +
      process.env.PROJECT +
      ' running on port ' +
      process.env.PORT
  );
});
