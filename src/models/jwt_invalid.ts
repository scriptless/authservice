/*
* NPM Imports
*/
import * as mongoose from 'mongoose';

/*
* Internal Imports
*/
import { IInvalidJWT, IInvalidJWTDocument } from '../interfaces/jwt_invalid';

const invalidJWTSchema = new mongoose.Schema({
    value: {
        type: String,
        required: true,
        unique: true,
    },
    expire: { type: Date, index: { expires: 0 } },
});

export type InvalidJWT = IInvalidJWT;
export const InvalidJWTScheme: mongoose.Model<IInvalidJWTDocument> = mongoose.model('InvalidJWT', invalidJWTSchema);
