/*
* NPM Imports
*/
import * as mongoose from 'mongoose';
import * as shortId from 'shortid';

/*
* Internal Imports
*/
import { ISessionDocument, ISession } from '../interfaces/session';

const userSchema = new mongoose.Schema({
    _id: { type: String, unique: true, default: shortId.generate },
    user: { type: String, required: true },
    audience: { type: String, required: true },
    jwt: { type: String, required: true },
    date: { type: Date, required: true, default: new Date() },
    expire: { type: Date, index: { expires: 0 } },
});

export type Session = ISession;
export const SessionScheme: mongoose.Model<ISessionDocument> = mongoose.model('Session', userSchema);
