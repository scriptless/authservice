//@ts-nocheck
/*
 * NPM Imports
 */
import * as mongoose from 'mongoose';
import * as argon2 from 'argon2';
import * as moment from 'moment';
/*
 * Internal Imports
 */
import { ICredentialDocument, ICredential } from '../interfaces/credential';
import { JWT } from '../interfaces/jwt';
import { Session, SessionScheme } from '../models/session';
import { verifyJWT, revokeJWT } from '../helper/jwt_helper';

export enum Roles {
  USER = 'USER',
  MOD = 'MOD',
  ADMIN = 'ADMIN',
}

const emailMatch =
  /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

const credsScheme = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    unique: true,
    auto: true,
    index: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    match: emailMatch,
  },
  username: { type: String, unique: true, sparse: true },
  password: { type: String, required: true },
  role: { type: String, required: true, default: Roles.USER },
  tokens: {
    emailCode: {
      code: { type: String, default: null },
      expiresAt: { type: Date, default: Date.now() },
    },
    forgotPassword: {
      code: { type: String, default: null },
      expiresAt: { type: Date, default: Date.now() },
    },
  },
  logins: [
    {
      date: {
        type: Date,
        required: true,
      },
      ipAddress: {
        type: String,
        required: true,
      },
    },
  ],
  registrationDate: { type: Date, default: Date.now() },
});

credsScheme.methods.checkPassword = async function (password: string) {
  return await argon2.verify(this.password, password);
};

credsScheme.methods.logLogin = function (ipAddress: string) {
  CredentialScheme.updateOne(
    { _id: this._id },
    { $addToSet: { logins: { date: new Date(), ipAddress } } },
    (_) => _
  );
};

credsScheme.methods.createSession = async function (
  jwt: string,
  role: string,
  remember: boolean
) {
  const session = new SessionScheme({
    user: this._id,
    audience: role,
    expire: new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 7), // now + 1 week in ms
    jwt,
  });

  await session.save().catch((err) => {
    throw err;
  });

  let expiresAt;

  switch (remember) {
    case true:
      expiresAt = moment().add(30, 'day').toDate();
      break;
    case false:
      expiresAt = moment().add(120, 'minute').toDate();
      break;
    default:
      break;
  }

  return {
    token: jwt,
    options: {
      domain: process.env.COOKIE_DOMAIN,
      expires: expiresAt,
      httpOnly: false,
      sameSite: 'strict',
      path: '/',
      secure: JSON.parse(process.env.COOKIE_SECURE),
    },
  };
};

credsScheme.methods.getSession = function (): Promise<any> {
  return new Promise((resolve) => {
    SessionScheme.findOne({ user: this._id }).then((result) => {
      resolve(result);
    });
  });
};

credsScheme.methods.destroySession = async function (): Promise<any> {
  return new Promise(async (resolve) => {
    const session = (await this.getSession()) as Session;
    const cookie = {
      options: {
        domain: process.env.COOKIE_DOMAIN,
        httpOnly: false,
        sameSite: 'strict',
        path: '/',
        secure: JSON.parse(process.env.COOKIE_SECURE),
      },
    };
    // if no session in db, try to delete cookie with authhandler
    if (!session) {
      return resolve(cookie);
    }
    const decoded = (await verifyJWT(session.jwt, session.audience)) as JWT;

    // @ts-ignore
    if (!decoded.err) {
      // token is valid, so revoke it
      await revokeJWT(session.jwt, decoded.exp);
    }
    // delete session
    await SessionScheme.deleteOne({
      user: this._id,
      audience: session.audience,
    });

    return resolve(cookie);
  });
};

export type Credential = ICredential;
export const CredentialScheme: mongoose.Model<ICredentialDocument> =
  mongoose.model('Credential', credsScheme);
