/*
 * NPM Imports
 */
import * as mailgun from 'mailgun-js';
import * as dotenv from 'dotenv';
import { log } from 'util';

dotenv.config();

const { MAILGUN_DOMAIN, MAILGUN_API_KEY } = process.env;
const mailer = mailgun({
  apiKey: MAILGUN_API_KEY,
  domain: MAILGUN_DOMAIN,
  host: 'api.eu.mailgun.net',
});

export const sendMail = (
  from: string,
  to: string,
  subject: string,
  text: string,
  vars: any[] = []
) => {
  let data = {
    from,
    to,
    subject,
    text,
  };

  vars.forEach((variable) => {
    data.text = data.text.replace(variable.name, variable.value);
  });

  mailer.messages().send(data, (error, body) => {
    if (error) return null;
    log('Sent email to ' + to + ' with subject ' + subject);
    return body;
  });
};

export const sendMailTemplate = (
  to: string,
  subject: string,
  template: string,
  vars: any[] = []
) => {
  let variables: any = {};
  vars.forEach((variable) => {
    variables['v:' + variable.name] = variable.value;
  });

  let data = {
    from: process.env.MAILGUN_FROM_EMAIL,
    to,
    subject,
    template,
    't:text': 'yes',
    ...variables,
  };

  mailer.messages().send(data, (error, body) => {
    if (error) return null;
    log('Sent email to ' + to + ' with subject ' + subject);
    return body;
  });
};
