/*
* NPM Imports
*/
import * as dotenv from 'dotenv';
import * as reCAPTCHA from 'recaptcha2';
import { log } from 'util';

dotenv.config();
 
var recaptcha = new reCAPTCHA({
  siteKey: process.env.GOOGLE_RECAPTCHA_PUBLICKEY,
  secretKey: process.env.GOOGLE_RECAPTCHA_PRIVATEKEY
});

export const verifyCaptcha = async (captcha: string, remoteip: string) => {
    return await recaptcha.validate(captcha, remoteip).then(() => true).catch(() => {
      log("Captcha invalid from ip address: " + remoteip);
      return false;
    });
}
