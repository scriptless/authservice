/*
* NPM Imports
*/
// @ts-ignore
import * as jwt from 'jsonwebtoken';

/*
* Internal Imports
*/
import { JWT } from '../interfaces/jwt';
import { Request } from 'express';
import { InvalidJWTScheme } from '../models/jwt_invalid';

export const createJWT = (user: string, email: string, audience: string): string => {
    return jwt.sign({
        iss: process.env.JWT_DOMAIN,
        sub: user,
        aud: audience,
        email,
    }, (global as any).privateKey, {
            algorithm: 'ES512',
            expiresIn: (60 * 60 * 24 * 7), // TODO: it's 1 week now
        });
}

export const dueForRenewal = (jwt: JWT): boolean => {
    const d = new Date();
    const seconds = Math.round(d.getTime() / 1000);
    const left = jwt.exp - seconds;

    return left < (60 * 60 * 24); // 1 day
};

export const extractJWT = async (req: Request, audience: string): Promise<any> => {
    return new Promise(async (resolve) => {
        // check for bearer token
        const bearerToken = extractBearerToken(req);
        if (!bearerToken) {
            resolve({ dec: null, raw: null });
            return;
        }

        const bearerDecoded: JWT = await verifyJWT(bearerToken, audience);
        // @ts-ignore
        if (bearerDecoded.err) {
            resolve({ jwt: null, raw: null });
            return;
        }
        resolve({ dec: bearerDecoded, token: bearerToken });
    });
};

export const extractBearerToken = (req: Request): string => {
    let token = "";
    let cookie = req.cookies["jwt_token"];
    if (req.headers && req.headers.authorization) {
        const parts = req.headers.authorization.split(' ');
        if (parts.length === 2 && parts[0] === 'Bearer') {
            token = parts[1];
        }
    } else if(cookie) {
        token = cookie;
    }
    return token;
};

export const verifyJWT = async (token: string, audience: string): Promise<any> => {
    return new Promise(async (resolve, _) => {
        jwt.verify(token, (global as any).publicKey, {
            algorithms: ['ES512'],
            audience,
        }, async (err: any, decoded: any) => {
            if (err) {
                resolve({ err })
                return;
            }

            // check for blacklist
            await InvalidJWTScheme.findOne({ value: token })
                .then(async (jwt: any) => {
                    if (jwt) {
                        resolve({ err: "JSON Web Token has been revoked" })
                    } else {
                        resolve(decoded as JWT)
                    }
                })
                .catch(err => {
                    resolve({ err })
                });
        })
    });
};

export const verifyJWTLax = async (token: string): Promise<any> => {
    return new Promise(async (resolve, _) => {
        jwt.verify(token, (global as any).publicKey, {
            algorithms: ['ES512'],
        }, async (err: any, decoded: any) => {
            if (err) {
                resolve({ err })
                return;
            }

            // check for blacklist
            await InvalidJWTScheme.findOne({ value: token })
                .then(async (jwt: any) => {
                    if (jwt) {
                        resolve({ err: "JSON Web Token has been revoked" })
                    } else {
                        resolve(decoded as JWT)
                    }
                })
                .catch(err => {
                    resolve({ err })
                });
        })
    });
};

export const revokeJWT = (token: string, expires: number): Promise<any> => {
    return new Promise(async (resolve) => {
        const invalidJWT = new InvalidJWTScheme({
            value: token,
            expire: new Date(expires * 1000), // convert seconds to ms
        });

        await invalidJWT.save()
            .then(() => {
                resolve({ invalidJWT });
            })
            .catch(err => {
                resolve({ err })
            });
    })
};
