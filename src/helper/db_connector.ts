import { connect, connection } from 'mongoose';

interface IDBCredentials {
  DATABASE_USER: string;
  DATABASE_PASSWORD: string;
  DATABASE_HOST: string;
  DATABASE_COLLECTION: string;
}

class DBConnector {
  private credentials: IDBCredentials;

  constructor(credentials: IDBCredentials) {
    this.credentials = credentials;
  }

  connect = () => {
    const url =
      'mongodb://' +
      this.credentials.DATABASE_USER +
      ':' +
      this.credentials.DATABASE_PASSWORD +
      '@' +
      this.credentials.DATABASE_HOST +
      '/' +
      this.credentials.DATABASE_COLLECTION +
      '?qa.connectionTimeout=30&qa.maxIdleTimeout=0';

    connect(url, { useNewUrlParser: true }, (err: any) => {
      if (err) {
        console.log(err);
        throw err;
      }
    });
  };

  disconnect = () => {
    connection.close();
  };
}

export { DBConnector, IDBCredentials };
